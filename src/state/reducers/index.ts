import { combineReducers } from "redux";
import { demoReducer } from "./demoReducer";

const reducers = combineReducers({
  demo: demoReducer,
});

const rootReducer = (state: any, action: any) => {
  switch (action.type) {
    case "RESET_STATE":
      return reducers(undefined, action);
    default:
      return reducers(state, action);
  }
};

// export default reducers;
export default rootReducer;
